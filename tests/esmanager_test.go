package tests

import (
	"errors"
	"fmt"
	"git.championtek.com.tw/go/esmanager"
	"testing"
)

var docs = map[int]string{
	1:  "Spider-Man : Homecoming",
	2:  "Ant-man and the Wasp",
	3:  "Avengers: Infinity War Part 2",
	4:  "Captain Marvel",
	5:  "Black Panther",
	6:  "Avengers: Infinity War",
	7:  "Thor: Ragnarok",
	8:  "Guardians of the Galaxy Vol 2",
	9:  "Doctor Strange",
	10: "Captain America: Civil War",
	11: "Ant-Man",
	12: "Avengers: Age of Ultron",
	13: "Guardians of the Galaxy",
	14: "Captain America: The Winter Soldier",
	15: "Thor: The Dark World",
	16: "Iron Man 3",
	17: "Marvel’s The Avengers",
	18: "Captain America: The First Avenger",
	19: "Thor",
	20: "Iron Man 2",
	21: "The Incredible Hulk",
	22: "Iron Man",
	23: "新北市板橋區雙十路二段",
	24: "新北市板橋區漢生東路",
	25: "新北市板橋區文化路",
	26: "台北市信義區",
	27: "台北市中山區",
	28: "台北市松山區",
	29: "台中市中山區",
}

const mapping = `
{
   "mappings": {
       "properties": {
			"id": {"type": "long"},
            "name": {
                "type": "text",
				"fields": {
					"keywordstring": {
						"type": "text",
						"analyzer": "chinese"
					},
					"edgengram": {
						"type": "text",
						"analyzer": "edge_ngram_analyzer",
						"search_analyzer": "edge_ngram_search_analyzer"
					},
					"completion": {
						"type": "completion"
					}
				},
				"analyzer": "standard"
           },
			"suggest": {
				"type": "completion"
			}
       }
   }
}`

func getConfig() esmanager.Config {
	return esmanager.Config{
		SetUrl:   []string{"http://52.196.34.12:9200/"},
		SetSniff: false,
	}
}

func TestOptionFormat(t *testing.T) {
	if optionsString, err := esmanager.Mgr.Options(esmanager.Settings, mapping); err != nil {
		t.Error(err)
	} else {
		fmt.Printf("options string : %s", optionsString)
	}
}

func TestConnection(t *testing.T) {
	dbConfig := getConfig()
	if err := esmanager.Mgr.Init(&dbConfig); err != nil {
		t.Fatal(err)
	}
	if esmanager.Mgr.GetClient() == nil {
		t.Fatal(errors.New("elastic client is nil\n"))
	}
	esmanager.Mgr.Close()
}

func TestIndexDoc(t *testing.T) {
	dbConfig := getConfig()
	if err := esmanager.Mgr.Init(&dbConfig); err != nil {
		t.Fatal(err)
	}
	if esmanager.Mgr.GetClient() == nil {
		t.Fatal(errors.New("elastic client is nil\n"))
	}

	indexDocs := []string{"marvels"}

	//建立doc陣列
	var movies []esmanager.Doc
	for key, doc := range docs {
		fmt.Println(doc)
		movie := esmanager.Doc{ID: key, Name: doc, Type: 1}
		movies = append(movies, movie)
	}

	//刪除原本索引
	for _, index := range indexDocs {
		if esmanager.Mgr.IsExists(index) {
			if err := esmanager.Mgr.DeleteIndex(index); err != nil {
				t.Fatal(err)
			} else {
				fmt.Printf("delete index : %s success\n", index)
			}
		}
	}

	//建立索引
	opts, _ := esmanager.Mgr.Options(esmanager.Settings, mapping)
	for _, index := range indexDocs {
		if err := esmanager.Mgr.CreateIndex(index, opts); err != nil {
			t.Fatal(err)
		} else {
			if esmanager.Mgr.IsExists(index) {
				fmt.Printf("create index : %s success\n", index)
			} else {
				t.Fatal(fmt.Errorf("create index : %s failed\n", index))
			}
		}
	}

	//建立資料
	for _, index := range indexDocs {
		movieDoc := esmanager.Documents{
			IndexName: index,
			Doc: movies,
		}

		if err := esmanager.Mgr.BulkCreate(movieDoc); err != nil {
			t.Fatal(err)
		} else {
			fmt.Printf("create %s data success\n", index)
		}
	}

	esmanager.Mgr.Close()
}

//If you update the docs for indexing, please execute TestIndexDoc first!
func TestQuery(t *testing.T)  {
	indexDocs := []string{"marvels"}
	dbConfig := getConfig()
	if err := esmanager.Mgr.Init(&dbConfig); err != nil {
		t.Fatal(err)
	}
	if esmanager.Mgr.GetClient() == nil {
		t.Fatal(errors.New("elastic client is nil\n"))
	}

	for _, index := range indexDocs {
		if esmanager.Mgr.IsExists(index) {
			if data, err := esmanager.Mgr.Query(index,"新北 板橋"); err != nil {
				t.Error(err)
			} else {
				//count data
				if len(data) != 3 {
					t.Error(fmt.Errorf("the count of result should be 3, however we got %d", len(data)))
				}
			}
		}
	}

	for _, index := range indexDocs {
		if esmanager.Mgr.IsExists(index) {
			if data, err := esmanager.Mgr.Query(index,"台北 區"); err != nil {
				t.Error(err)
			} else {
				//count data
				if len(data) != 3 {
					t.Error(fmt.Errorf("the count of result should be 3, however we got %d", len(data)))
				}
			}
		}
	}
	esmanager.Mgr.Close()
}

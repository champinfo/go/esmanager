package esmanager

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/olivere/elastic/v7"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

const Settings = `
{
"settings": {
		"index": {
			"analysis": {
				"filter": {},
				"analyzer": {
					"keyword_analyzer": {
						"filter": [
							"lowercase",
							"asciifolding",
							"trim",
							"cjk_bigram",
							"cjk_width"
						],
						"char_filter": [],
						"type": "custom",
						"tokenizer": "keyword"
					},
					"edge_ngram_analyzer": {
						"filter": [
							"lowercase",
							"cjk_bigram",
							"cjk_width"
						],
						"tokenizer": "edge_ngram_tokenizer"
					},
					"edge_ngram_search_analyzer": {
						"filter": [
							"cjk_bigram",
							"cjk_width"
						],
						"tokenizer": "lowercase"
					},
					"chinese": {
						"type": "smartcn"
					}
				},
				"tokenizer": {
					"edge_ngram_tokenizer": {
						"type": "edge_ngram",
						"min_gram": 2,
						"max_gram": 5,
						"token_chars": [
							"letter"
						]
					}
				}
			}
		}
	}
}
`

type ESManager interface {
	Options(option ...string) (string, error)
	Init(config *Config) error
	GetClient() *elastic.Client
	Close()
	Terminate()
	BulkCreate(doc Documents) error
	IsExists(indexName string) bool
	CreateIndex(indexName string, mapping string) error
	DeleteIndex(indexName string) error
	Query(indexName string, term string) ([]*Doc, error)
}

type Config struct {
	SetSniff bool     `json:"SetSniff"  yaml:"SetSniff"`
	SetUrl   []string `json:"SetUrl"  yaml:"SetUrl"`
}

type Documents struct {
	IndexName string `json:"indexName"`
	Doc       []Doc
}

type Doc struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Type int    `json:"type"`
}

type Opts struct {
	Settings interface{} `json:"settings"`
	Mappings interface{} `json:"mappings"`
}

type esmanager struct {
	Client *elastic.Client
	//Host   string
}

var Mgr ESManager

func init() {
	Mgr = &esmanager{}
}

func (esm *esmanager) Options(options ...string) (string, error) {
	opts := Opts{}
	for _, option := range options {
		var js map[string]interface{}
		if err := json.Unmarshal([]byte(option), &js); err != nil {
			return "", err
		}
		if js["settings"] != nil {
			opts.Settings = js["settings"]
		}
		if js["mappings"] != nil {
			opts.Mappings = js["mappings"]
		}
	}

	if  merge, err := json.Marshal(opts); err != nil {
		return "", err
	} else {
		return string(merge), nil
	}
}

func (esm *esmanager) Init(config *Config) error {
	if config == nil || len(config.SetUrl) == 0 {
		fmt.Println("config or config.SetUrl can not be nil")
		return errors.New("config or url can not be nil")
	}
	option := []elastic.ClientOptionFunc{
		elastic.SetSniff(config.SetSniff),
		elastic.SetURL(config.SetUrl...),
	}
	if client, err := elastic.NewClient(option...); err != nil {
		return err
	} else {
		for _, host := range config.SetUrl {
			if _, _, err := client.Ping(host).Do(context.Background()); err != nil {
				fmt.Printf("host %#v is not reachable with error : %#v\n", host, err)
			}
		}
		esm.Client = client
		return nil
	}
}

func (esm *esmanager) GetClient() *elastic.Client {
	return esm.Client
}

func (esm *esmanager) Terminate() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	signal.Notify(c, syscall.SIGKILL)
	go func() {
		<-c
		esm.Client.Stop()
		fmt.Println("Elastic is terminated")
		os.Exit(0)
	}()
}

//關閉背景程式，並且停止客戶端運作
func (esm *esmanager) Close() {
	esm.Client.Stop()
}

//批次寫入
func (esm *esmanager) BulkCreate(doc Documents) error {
	bulkRequest := esm.Client.Bulk()
	for _, document := range doc.Doc {
		doc := elastic.NewBulkIndexRequest().Index(doc.IndexName).Id(strconv.Itoa(document.ID)).Doc(document)
		bulkRequest = bulkRequest.Add(doc)
	}

	if response, err := bulkRequest.Do(context.Background()); err != nil {
		return err
	} else {
		if len(response.Failed()) > 0 {
			fmt.Printf("error items(%#v) : %#v\n", len(response.Failed()), response.Errors)
		}
		return nil
	}
}

//檢查index
func (esm *esmanager) IsExists(indexName string) bool {
	flag := true
	if exist, err := esm.Client.IndexExists(indexName).Do(context.Background()); err != nil {
		fmt.Printf("error : %#v\n", err)
		flag = false
	} else {
		if !exist {
			fmt.Printf("indexName : %#v maybe incorrect\n", indexName)
			flag = false
		}
	}
	return flag
}

//建立index
func (esm *esmanager) CreateIndex(indexName string, mapping string) error {
	if result, err := esm.Client.CreateIndex(indexName).BodyString(mapping).Do(context.Background()); err != nil {
		fmt.Printf("error : %#v\n", err)
		return err
	} else {
		fmt.Printf("create indices result : %#v\n", result.Index)
		return nil
	}
}

//刪除index
func (esm *esmanager) DeleteIndex(indexName string) error {
	if response, err := esm.Client.DeleteIndex(indexName).Do(context.Background()); err != nil {
		fmt.Printf("error : %#v\n", err)
		return err
	} else {
		fmt.Printf("delete indices result : %#v\n", response.Acknowledged)
		return nil
	}
}

//Query
func (esm *esmanager) Query(indexName string, term string) ([]*Doc, error) {
	query := elastic.NewQueryStringQuery(term)
	query = query.DefaultOperator("AND")
	//src, err := query.Source()
	//if err != nil {
	//	return nil, err
	//}
	//request, err := json.MarshalIndent(src, "", "  ")
	//if err != nil {
	//	return nil, err
	//}
	//fmt.Println(string(request))

	search := esm.Client.Search().Index(indexName).Pretty(true)
	response, err := search.Query(query).Do(context.Background())
	if err != nil {
		return nil, err
	}
	if response == nil || response.TotalHits() == 0 {
		return nil, nil
	}

	var docs []*Doc
	for _, hit := range response.Hits.Hits {
		doc := new(Doc)
		if err := json.Unmarshal(hit.Source, doc); err != nil {
			return nil, err
		}
		docs = append(docs, doc)
	}
	return docs, nil
}
